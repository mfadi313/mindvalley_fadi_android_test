package com.fadi.mindvalley_fadi_android_test;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AppConnections {

    public static ArrayList<Card> getProducts() {
        try {
            String str_response = AppCommon.post(AppCommon.URL_PRODUCTS, null);
            if (str_response != null) {
                JSONObject response = new JSONObject(str_response);
                if (!response.isNull("success") && response.getBoolean("success")) {
                    ArrayList<Card> list = new ArrayList<>();
                    JSONArray results = response.getJSONObject("metadata").getJSONArray("results");
                    for (int i = 0; i < results.length(); i++) {
                        list.add(
                                new Card(
                                        results.getJSONObject(i).getJSONObject("data").getString("name"),
                                        results.getJSONObject(i).getJSONArray("images")
                                                .getJSONObject(0).getString("path")
                                )
                        );
                    }
                    return list;
                }
            }
        } catch (JSONException e) {
            //e.printStackTrace();
        }
        return null;
    }
}