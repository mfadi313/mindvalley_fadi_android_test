package com.fadi.mindvalley_fadi_android_test;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.etsy.android.grid.util.DynamicHeightImageView;
import com.fadi.mindvalley_fadi_android_test.library.CacheManager;
import com.fadi.mindvalley_fadi_android_test.library.Downloader;

import java.util.ArrayList;

public class CardsAdapter extends ArrayAdapter<Card> {

    Activity activity;
    int resource;
    ArrayList<Card> data;

    public CardsAdapter(Activity activity, int resource, ArrayList<Card> objects) {
        super(activity, resource, objects);

        this.activity = activity;
        this.resource = resource;
        this.data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final CardHolder holder;

        if (row == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            row = inflater.inflate(resource, parent, false);

            holder = new CardHolder();
            holder.image = (DynamicHeightImageView) row.findViewById(R.id.image);

            holder.progressBar = (ProgressBar) row.findViewById(R.id.progress);
            holder.progressBar.setVisibility(View.VISIBLE);

            holder.title = (TextView) row.findViewById(R.id.title);

            row.setTag(holder);
        } else {
            holder = (CardHolder) row.getTag();
        }

        final Card card = data.get(position);

        CacheManager.with(getContext())
                .load(card.getDefaultImageUrl())
                .intoImageView(holder.image)
                .setOnDownloadProgressListener(new Downloader.OnDownloadProgressListener() {
                    @Override
                    public void onProgressUpdate(int totalSize, int downloadedSize) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                holder.progressBar.setVisibility(View.VISIBLE);
                            }
                        });
                        //Log.e("ttt", "Total = " + totalSize);
                        //Log.e("ttt", "Downloaded = " + downloadedSize);
                    }
                })
                .setOnDownloadFinishListener(new Downloader.OnDownloadFinishListener() {
                    @Override
                    public void onDownloadSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onDownloadFailed() {
                        holder.progressBar.setVisibility(View.GONE);
                        holder.image.setImageResource(R.drawable.pink_star);
                    }
                });

        holder.image.setHeightRatio(1.0);
        holder.title.setText(card.getName());

        return row;
    }

    static class CardHolder {
        DynamicHeightImageView image;
        ProgressBar progressBar;
        TextView title;
    }
}