package com.fadi.mindvalley_fadi_android_test;

public class Card {

    private String name;
    private String defaultImageUrl;

    public Card(String name, String defaultImageUrl) {
        this.name = name;
        this.defaultImageUrl = defaultImageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefaultImageUrl() {
        return defaultImageUrl;
    }

    public void setDefaultImageUrl(String defaultImageUrl) {
        this.defaultImageUrl = defaultImageUrl;
    }
}
