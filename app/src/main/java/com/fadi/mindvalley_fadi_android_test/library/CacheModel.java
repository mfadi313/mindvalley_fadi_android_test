package com.fadi.mindvalley_fadi_android_test.library;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import java.io.File;

class CacheModel extends SQLiteOpenHelper {

    public static final String CACHE_URL = "cache_url";
    public static final String CACHE_PATH = "cache_path";
    private static final String PACKAGE_NAME = "com.fadi.mindvalley_fadi_android_test.library";
    private static final String DATABASE_NAME = PACKAGE_NAME + ".db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_CACHE = "table_cache";
    private static final String CACHE_LAST_RETRIEVED = "cache_last_retrieved";

    public CacheModel(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Check if the database exist and can be read.
     *
     * @return true if it exists and can be read, false if it doesn't
     */
    private static boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(DATABASE_NAME, null,
                    SQLiteDatabase.OPEN_READONLY);
            checkDB.close();
        } catch (SQLiteException e) {
            // database doesn't exist yet.
        }
        return checkDB != null;
    }

    private static boolean checkDataBase(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table " + TABLE_CACHE + " ( " +
                        "_id integer primary key autoincrement, " +
                        CACHE_URL + " text, " +
                        CACHE_PATH + " text, " +
                        CACHE_LAST_RETRIEVED + " timestamp default current_timestamp " +
                        ");"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void removeFromDataBase(String url) {
        getWritableDatabase().delete(TABLE_CACHE, CACHE_URL + " = ?", new String[]{url});
    }

    public long saveToDataBase(String url, String path) {
        ContentValues values = new ContentValues();
        values.put(CACHE_URL, url);
        values.put(CACHE_PATH, path);

        return getWritableDatabase().insertOrThrow(TABLE_CACHE, null, values);
    }

    public String getFromDataBase(String url) {
        String returnValue = null;
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TABLE_CACHE);
        Cursor c = qb.query(db, null, CACHE_URL + " = ?", new String[]{url}, null, null, null);
        if (c != null && c.moveToFirst()) {
            returnValue = c.getString(c.getColumnIndex(CACHE_PATH));

            Long tsLong = System.currentTimeMillis() / 1000;
            ContentValues values = new ContentValues();
            values.put(CACHE_LAST_RETRIEVED, tsLong.toString());
            db.update(
                    TABLE_CACHE,
                    values,
                    CACHE_URL + " = ?",
                    new String[]{url}
            );

            c.close();
        }
        return returnValue;
    }

    public Cursor getAllFilesSortedByOld() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TABLE_CACHE);
        return qb.query(db, null, null, null, null, null, CACHE_LAST_RETRIEVED + " DESC ");
    }
}