package com.fadi.mindvalley_fadi_android_test.library;

import android.content.Context;
import android.database.Cursor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CacheManager {
    private static CacheModel singletonCacheModel;
    private static long MAX_SIZE = 200 * 1024 * 1024; // 200 MB

    /***************************************************************/

    public CacheManager(Context context) {
        if (singletonCacheModel == null) {
            singletonCacheModel = new CacheModel(context);
        }
    }

    public static Downloader with(Context context) {
        if (singletonCacheModel == null) {
            singletonCacheModel = new CacheModel(context);
        }
        return new Downloader(context, singletonCacheModel);
    }

    /***************************************************************/

    public static long getMaxSize() {
        return MAX_SIZE;
    }

    public static void setMaxSize(long maxSize) {
        MAX_SIZE = maxSize;
    }

    /**
     * Return the size of a directory in bytes
     */
    private static long getDirSize(File dir) {

        if (dir.exists()) {
            long result = 0;
            File[] fileList = dir.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                // Recursive call if it's a directory
                if (fileList[i].isDirectory()) {
                    result += getDirSize(fileList[i]);
                } else {
                    // Sum the file size in bytes
                    result += fileList[i].length();
                }
            }
            return result; // return the file size
        }
        return 0;
    }

    public void cacheData(Context context, byte[] data, String url) throws IOException {

        long size = getDirSize(context.getFilesDir());
        long newSize = data.length + size;

        if (newSize > MAX_SIZE) {
            cleanDir(newSize - MAX_SIZE);
        }

        String urlPath = url.substring(0, url.lastIndexOf("/"))
                .replace("http://", "")
                .replace("https://", "")
                .replace("/", File.separator);
        String fileName = url.substring(url.lastIndexOf("/") + 1);

        File fileDir = new File(context.getFilesDir(), urlPath);
        fileDir.mkdirs();
        File file = new File(fileDir, fileName);

        FileOutputStream os = new FileOutputStream(file);
        try {
            os.write(data);
        } finally {
            os.flush();
            os.close();
            singletonCacheModel.saveToDataBase(url, file.getPath());
        }
    }

    public byte[] retrieveData(Context context, String url) throws IOException {

        String filePath = singletonCacheModel.getFromDataBase(url);
        if (filePath != null) {
            File file = new File(filePath);

            if (!file.exists()) {
                // Data doesn't exist
                return null;
            }

            byte[] data = new byte[(int) file.length()];
            FileInputStream is = new FileInputStream(file);
            try {
                is.read(data);
            } finally {
                is.close();
            }

            return data;
        }
        return null;
    }

    public void cleanAllFiles(Context context) {
        cleanDir(getDirSize(context.getFilesDir()));
    }

    private void cleanDir(long bytes) {

        long bytesDeleted = 0;
        Cursor c = singletonCacheModel.getAllFilesSortedByOld();
        if (c != null && c.moveToFirst()) {
            do {
                File file = new File(c.getString(c.getColumnIndex(CacheModel.CACHE_PATH)));
                bytesDeleted += file.length();
                singletonCacheModel.removeFromDataBase(c.getString(c.getColumnIndex(CacheModel.CACHE_URL)));
                file.delete();
            }
            while (bytesDeleted < bytes && c.moveToNext());
        }
    }
}
