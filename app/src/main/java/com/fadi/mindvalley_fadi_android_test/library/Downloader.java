package com.fadi.mindvalley_fadi_android_test.library;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

public class Downloader {

    private static HashMap<String, Downloader> allOnGoingDownloaders = new HashMap<>();

    private Context context;
    private CacheModel cacheModel;
    private ArrayList<OnDownloadFinishListener> onDownloadFinishListeners;
    private ArrayList<OnDownloadProgressListener> OnDownloadProgressListeners;

    private AsyncTask<String, Integer, String> downloadTask;
    private int downloadSize;
    private int downloadedAmountSize;
    private boolean success = false;
    public Downloader(Context context, CacheModel cacheModel) {
        this.context = context;
        this.cacheModel = cacheModel;
        onDownloadFinishListeners = new ArrayList<>();
        OnDownloadProgressListeners = new ArrayList<>();
    }

    public int getDownloadedAmountSize(String url) {
        int downloadedAmountSize = 0;
        if (allOnGoingDownloaders.containsKey(url)) {
            downloadedAmountSize = allOnGoingDownloaders.get(url).downloadedAmountSize;
        }
        return downloadedAmountSize;
    }

    public int getDownloadSize(String url) {
        int downloadSize = 0;
        if (allOnGoingDownloaders.containsKey(url)) {
            downloadSize = allOnGoingDownloaders.get(url).downloadSize;
        }
        return downloadSize;
    }

    public boolean isDownloadComplete(String url) {
        String path = cacheModel.getFromDataBase(url);
        return path != null;
    }

    public IntoRequest load(final String url) {

        if (allOnGoingDownloaders.containsKey(url)) {
            if (allOnGoingDownloaders.get(url) != this) {
                return allOnGoingDownloaders.get(url).load(url);
            }
        }

        if (!isDownloadComplete(url)) {
            this.downloadSize = 0;
            this.downloadedAmountSize = 0;
            this.downloadTask = new AsyncTask<String, Integer, String>() {
                @Override
                protected String doInBackground(String... params) {
                    int count;
                    try {
                        URL connection_url = new URL(url);
                        URLConnection connection = connection_url.openConnection();
                        connection.connect();
                        // getting file length
                        downloadSize = connection.getContentLength();

                        // input stream to read file - with 8k buffer
                        InputStream input = new BufferedInputStream(connection_url.openStream(), 8192);
                        ByteArrayOutputStream out = new ByteArrayOutputStream();

                        byte data[] = new byte[1024];
                        while ((count = input.read(data)) != -1) {
                            downloadedAmountSize += count; // percentage = (int) ((downloadedAmountSize * 100) / downloadSize)
                            out.write(data, 0, count);
                            for (OnDownloadProgressListener onDownloadProgressListener : OnDownloadProgressListeners) {
                                onDownloadProgressListener.onProgressUpdate(downloadSize, downloadedAmountSize);
                            }
                        }
                        out.flush();
                        out.close();

                        input.close();

                        new CacheManager(context).cacheData(context, out.toByteArray(), url);

                        return String.valueOf(true);
                    } catch (Exception e) {
                        //Log.e("Error: ", e.getMessage());
                    }
                    return String.valueOf(false);
                }

                @Override
                protected void onPostExecute(String result) {
                    if (Boolean.valueOf(result)) {
                        for (OnDownloadFinishListener onDownloadFinishListener : onDownloadFinishListeners) {
                            onDownloadFinishListener.onDownloadSuccess();
                        }
                        success = true;
                    } else {
                        for (OnDownloadFinishListener onDownloadFinishListener : onDownloadFinishListeners) {
                            onDownloadFinishListener.onDownloadFailed();
                        }
                    }
                    //allOnGoingDownloaders.remove(url);
                }
            }.execute(null, null, null);
            allOnGoingDownloaders.put(url, this);
        } else {
            success = true;
        }
        return new IntoRequest(context, cacheModel, this, url);
    }

    public void stopLoading(String url) {
        if (allOnGoingDownloaders.containsKey(url)) {
            allOnGoingDownloaders.get(url).downloadTask.cancel(true);
            for (OnDownloadFinishListener onDownloadFinishListener : allOnGoingDownloaders.get(url).onDownloadFinishListeners) {
                onDownloadFinishListener.onDownloadFailed();
            }
        }
    }

    public IntoRequest reload(String url) {
        if (allOnGoingDownloaders.containsKey(url)) {
            allOnGoingDownloaders.get(url).downloadTask.cancel(true);
            return allOnGoingDownloaders.get(url).load(url);
            //allOnGoingDownloaders.remove(url);
        }
        return load(url);
    }

    public Downloader setOnDownloadFinishListener(OnDownloadFinishListener onDownloadFinishListener) {
        this.onDownloadFinishListeners.add(onDownloadFinishListener);
        if (success) {
            onDownloadFinishListener.onDownloadSuccess();
        }
        return this;
    }

    public Downloader setOnDownloadProgressListener(OnDownloadProgressListener onDownloadProgressListener) {
        this.OnDownloadProgressListeners.add(onDownloadProgressListener);
        return this;
    }

    /***************************************************************/

    public interface OnDownloadFinishListener {
        void onDownloadSuccess();

        void onDownloadFailed();
    }

    /***************************************************************/

    public interface OnDownloadProgressListener {
        void onProgressUpdate(int totalSize, int downloadedSize);
    }
}
