package com.fadi.mindvalley_fadi_android_test.library;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

public class IntoRequest {

    Downloader downloader;
    Context context;
    CacheModel cacheModel;
    String url;

    public IntoRequest(Context context, CacheModel cacheModel, Downloader downloader, String url) {
        this.context = context;
        this.cacheModel = cacheModel;
        this.downloader = downloader;
        this.url = url;
    }

    public Downloader intoImageView(final ImageView imageView) {

        downloader.setOnDownloadFinishListener(new Downloader.OnDownloadFinishListener() {
            @Override
            public void onDownloadSuccess() {
                try {
                    byte[] data = new CacheManager(context).retrieveData(context, url);
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    imageView.setImageBitmap(bitmap);
                } catch (Exception e) {
                    //e.printStackTrace();
                }

            }

            @Override
            public void onDownloadFailed() {
            }
        });

        return downloader;
    }
}
