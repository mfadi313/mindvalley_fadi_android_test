package com.fadi.mindvalley_fadi_android_test;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.fadi.mindvalley_fadi_android_test.library.CacheManager;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.grid_view)
    StaggeredGridView gridView;

    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        final ProgressDialog progressDialog = new ProgressDialog(this,
                R.style.AppTheme_Dark_Dialog);
        new AsyncTask<String, Void, String>() {

            @Override
            protected void onPreExecute() {
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.setMessage(getResources().getString(R.string.retrieving_products));
                progressDialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                ArrayList<Card> products = AppConnections.getProducts();
                if (products == null) {
                    return String.valueOf(false);
                }
                AppCommon.setProducts(products);
                return String.valueOf(true);
            }

            @Override
            protected void onPostExecute(String result) {
                progressDialog.dismiss();
                if (!Boolean.valueOf(result)) {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.retrieving_products_failed),
                            Toast.LENGTH_LONG
                    ).show();
                    startActivity(new Intent(MainActivity.this, InfoActivity.class));
                    MainActivity.this.finish();
                } else {
                    final CardsAdapter adapter = new CardsAdapter(MainActivity.this, R.layout.list_item_sample, AppCommon.getProducts());
                    gridView.setAdapter(adapter);
                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String url = adapter.getItem(position).getDefaultImageUrl();
                            String title = adapter.getItem(position).getName();

                            if (view.getTag(R.string.downloading_canceled_for) != null && view.getTag(R.string.downloading_canceled_for).toString().equals(getResources().getString(R.string.downloading_canceled_for))) {
                                CacheManager.with(MainActivity.this).reload(url);
                                view.setTag(R.string.downloading_canceled_for, getResources().getString(R.string.redownloading_for));
                                Toast.makeText(MainActivity.this,
                                        getResources().getString(R.string.redownloading_for) + title,
                                        Toast.LENGTH_SHORT).show();
                            } else if (!CacheManager.with(MainActivity.this).isDownloadComplete(url)) {
                                CacheManager.with(MainActivity.this).stopLoading(url);
                                view.setTag(R.string.downloading_canceled_for, getResources().getString(R.string.downloading_canceled_for));
                                Toast.makeText(MainActivity.this,
                                        getResources().getString(R.string.downloading_canceled_for) + title,
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                Intent i = new Intent(MainActivity.this, FullScreenViewActivity.class);
                                i.putExtra("position", position);
                                MainActivity.this.startActivity(i);
                            }
                        }
                    });

                    gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(AbsListView view, int scrollState) {

                        }

                        @Override
                        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                            int topRowVerticalPosition =
                                    (gridView == null || gridView.getChildCount() == 0) ?
                                            0 : gridView.getChildAt(0).getTop();
                            swipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
                        }
                    });
                }
            }
        }.execute(null, null, null);

        FloatingActionButton fab_add_fields = (FloatingActionButton) findViewById(R.id.fab_reload);
        fab_add_fields.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
                adb.setMessage(getResources().getString(R.string.reset_confirm));
                adb.setIcon(android.R.drawable.ic_dialog_alert);
                adb.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new CacheManager(MainActivity.this).cleanAllFiles(MainActivity.this);
                        startActivity(new Intent(MainActivity.this, MainActivity.class));
                        MainActivity.this.finish();
                    }
                });
                adb.setNegativeButton(getResources().getString(R.string.cancel), null);
                adb.show();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);

    }

    @Override
    public void onRefresh() {
        new AsyncTask<String, Void, String>() {

            @Override
            protected void onPreExecute() {
                swipeRefreshLayout.setRefreshing(true);
            }

            @Override
            protected String doInBackground(String... params) {
                ArrayList<Card> products = AppConnections.getProducts();
                if (products == null) {
                    return String.valueOf(false);
                }
                AppCommon.setProducts(products);
                return String.valueOf(true);
            }

            @Override
            protected void onPostExecute(String result) {
                final CardsAdapter adapter = new CardsAdapter(MainActivity.this, R.layout.list_item_sample, AppCommon.getProducts());
                gridView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                if (!Boolean.valueOf(result)) {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.refresh_failed),
                            Toast.LENGTH_SHORT
                    ).show();
                    startActivity(new Intent(MainActivity.this, InfoActivity.class));
                    MainActivity.this.finish();
                } else {
                    Toast.makeText(
                            getApplicationContext(),
                            getResources().getString(R.string.refresh_completed),
                            Toast.LENGTH_SHORT
                    ).show();
                }
            }
        }.execute(null, null, null);
    }
}
