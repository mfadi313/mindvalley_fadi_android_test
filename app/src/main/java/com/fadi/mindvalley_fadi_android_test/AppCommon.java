package com.fadi.mindvalley_fadi_android_test;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

public class AppCommon extends Application {

    public static final String SERVER_ADDRESS = "https://www.zalora.com.my/mobile-api/";

    public static final String URL_PRODUCTS = SERVER_ADDRESS + "women/clothing";
    private static ArrayList<Card> products;
    private static Context context;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    public static ArrayList<Card> getProducts() {
        return products;
    }

    public static void setProducts(ArrayList<Card> mProducts) {
        products = mProducts;
    }

    public static Context getContext() {
        return context;
    }

    public static String post(String url, List<NameValuePair> nameValuePairs) {
        HttpClient httpClient = new DefaultHttpClient();
        HttpContext localContext = new BasicHttpContext();
        HttpPost httpPost = new HttpPost(url);
        //Log.e("url", url);

        try {
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            //httpPost.setEntity(entity);

            if (nameValuePairs != null) {
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
                for (int index = 0; index < nameValuePairs.size(); index++) {
                    entity.addPart(nameValuePairs.get(index).getName(), new StringBody(nameValuePairs.get(index).getValue()));
                }
            }

            HttpResponse response = httpClient.execute(httpPost, localContext);
            String json_string = EntityUtils.toString(response.getEntity());
            //Log.e("json", json_string);
            return json_string;
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return null;
    }

    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        sharedPreferences = getSharedPreferences("app_prefs", Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }
}
